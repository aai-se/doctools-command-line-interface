##############################################################
#
# Author: Bren Sapience
# Date: Dec 2019
# Scope: Provide CLI functions around Doctools
#
# Call Examples:
#
#   python3 ./
#   python3 ./
#
##############################################################

import argparse
import sys
sys.path.insert(1, './logics')
import LanguageLogics
sys.path.insert(1, './libs')
import DataUtils

#Allowed Operations - should be lower case
SupportedOperations = ['list']

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Operation should be available in all modules
    parser.add_argument("-o","--operation", type=str,default = "list", help = "<list>:list languages", dest = "OPERATION")
    parser.add_argument("-c","--csv",default = False, help = "Return results in csv format (Default: False)", dest = "CsvOutput")

    OPTIONS = parser.parse_args()

    #Check that operation is Supported
    if OPTIONS.OPERATION.lower() not in SupportedOperations:
        parser.error('Operation passed (-o) is not supported: '+OPTIONS.OPERATION)
        
    DataUtils.DisplayURLandPort()
    ###
    # list operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[0]: #list
        LanguageLogics.list(OPTIONS.CsvOutput)
