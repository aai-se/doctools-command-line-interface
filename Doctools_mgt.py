##############################################################
#
# Author: Bren Sapience
# Date: May 2020
# Scope: Provide CLI functions around Doctools
#
# Call Examples:
#
#   python3 ./
#   python3 ./
#
##############################################################

import argparse
import sys
sys.path.insert(1, './logics')
sys.path.insert(1, './libs')
import ClassifiersLogics
import AllInOneLogics
import LanguageLogics
import DataUtils

#Allowed Operations - should be lower case
SupportedOperations = ['list','import','export']

def checkParameter(Value,ValueOptions):
    if not Value in ValueOptions:
        print("\t Error: "+ Value +" is not a valid option. Possible Values: "+str(ValueOptions))
        exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Operation should be available in all modules
    parser.add_argument("-o","--operation", type=str,default = "list", help = "<import,export>: classifiers", dest = "OPERATION")
    parser.add_argument("-c","--classifier", type=str,default = "", help = "(Optional) CSV List of Classifier IDs", dest = "CLASSIFIERID")
    parser.add_argument("-x","--outputfolder", type=str,default = "", help = "Output Folder to store exported Classifier", dest = "OUTPUTPATH")
    parser.add_argument("-i","--inputfile", type=str,default = "", help = "Classifier icem file to import", dest = "INPUTFILE")
    parser.add_argument("-r","--replace",action='store_true',default = False, help = "Replace Classifier (if exists, when importing)",dest = "REPLACE")



    OPTIONS = parser.parse_args()

    #Check that operation is Supported
    if OPTIONS.OPERATION.lower() not in SupportedOperations:
        parser.error('Operation passed (-o) is not supported: '+OPTIONS.OPERATION)

    DataUtils.DisplayURLandPort()

    ###
    # list operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[0]: #list
        ClassifiersLogics.list()

    ###
    # import operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[1]: #import
        if not OPTIONS.INPUTFILE:
            parser.error('You need to pass parameter -i or (--inputfile)')

        Flag = 0
        if OPTIONS.REPLACE:
            Flag = 1

        ClassifiersLogics.importClassifier(OPTIONS.INPUTFILE,Flag)

    ###
    # export operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[2]: #export
        if not OPTIONS.OUTPUTPATH:
            parser.error('You need to pass parameter -x or (--outputfolder)')

        if not OPTIONS.CLASSIFIERID:
            OPTIONS.CLASSIFIERID = ClassifiersLogics.listInternal()
            print("No Classifier ID passed. Exporting all Classifiers: \n"+OPTIONS.CLASSIFIERID)
            #parser.error('You need to pass parameter -c or (--classifier)')

        ClassifiersLogics.export(OPTIONS.CLASSIFIERID,OPTIONS.OUTPUTPATH)
