import json
import pandas as pd


def GetListAsList(jsonResults):
    #{'page': {'offset': 0, 'total': 1, 'totalFilter': 1}, 'list': [{'id': '1', 'type': 'ATTENDED_BOT_RUNNER', 'hostName': 'EC2AMAZ-4PFQG1I', 'userId': '', 'userName': '', 'status': 'CONNECTED',
    # 'poolName': '', 'fullyQualifiedHostName': '-', 'updatedBy': 'iqbot', 'updatedOn': '2019-11-18T05:35:39.154Z'}]}

    itemList = jsonResults['result']['response']
    Results = ""
    for item in itemList:
        if(Results ==""):
            Results = item
        else:
            Results = Results +","+item

    return Results


def GetListAsCsv(jsonResults):
    #{'page': {'offset': 0, 'total': 1, 'totalFilter': 1}, 'list': [{'id': '1', 'type': 'ATTENDED_BOT_RUNNER', 'hostName': 'EC2AMAZ-4PFQG1I', 'userId': '', 'userName': '', 'status': 'CONNECTED',
    # 'poolName': '', 'fullyQualifiedHostName': '-', 'updatedBy': 'iqbot', 'updatedOn': '2019-11-18T05:35:39.154Z'}]}

    out_df = pd.DataFrame(columns=['ID'])

    itemList = jsonResults['result']['response']

    for item in itemList:
        ID = item


        new_row = {'ID':ID}
        out_df = out_df.append(new_row, ignore_index=True)

    return out_df.to_csv(index=False)

def truncate(n, decimals=0):
    multiplier = 10 ** decimals
    return (int(n * multiplier) / multiplier)*100

def GetTrainResponseAsText(jsonResults):
#{'accuracy': 1.0, 'precision': 1.0, 'fScore': 1.0, 'recall': 1.0, 'stdDev': 0.0, 'resultMatrix': {'FORM1095-C': {'FORM1095-C': 4, 'FORM1095-B': 0, 'FORM1094-C': 0, 'FORM1094-B': 0, 'Unknown': 0}, 'FORM1095-B': {'FORM1095-C': 0, 'FORM1095-B': 5, 'FORM1094-C': 0, 'FORM1094-B': 0, 'Unknown': 0}, 'FORM1094-C': {'FORM1095-C': 0, 'FORM1095-B': 0, 'FORM1094-C': 5, 'FORM1094-B': 0, 'Unknown': 0}, 'FORM1094-B': {'FORM1095-C': 0, 'FORM1095-B': 0, 'FORM1094-C': 0, 'FORM1094-B': 5, 'Unknown': 0}, 'Unknown': {'FORM1095-C': 0, 'FORM1095-B': 0, 'FORM1094-C': 0, 'FORM1094-B': 0, 'Unknown': 0}}}


    print("\t\t => Accuracy:"+str(jsonResults['accuracy']))
    print("\t\t => Precision:"+str(jsonResults['precision']))
    print("\t\t => FScore:"+str(jsonResults['fScore']))
    print("\t\t => Recall:"+str(jsonResults['recall']))
    print("\t\t => StdDeviation:"+str(jsonResults['stdDev']))

def GetClassifyResponseAsText(jsonResults):
    #[{'pageNumber': 1, 'similarity': 0.0, 'classifications': [{'confidence': 0.8780161835348611, 'label': 'FORM1094-B'}, {'confidence': 0.059413442888834544, 'label': 'FORM1094-C'}, {'confidence': 0.03715104828142546, 'label': 'FORM1095-B'}, {'confidence': 0.03582668413562995, 'label': 'FORM1095-C'}]}]
    Columns = ['label','confidence']

    out_df = pd.DataFrame(columns=Columns)

    itemList = jsonResults[0]['classifications']
    print("\n\t Label      | Confidence Score")
    for item in itemList:
        CONFIDENCE = item['confidence']
        LABEL = item['label']
        CONFIDENCE_READABLE = truncate(CONFIDENCE,2)
        print("\t "+LABEL+" | "+str(CONFIDENCE_READABLE)+"%")

        new_row = {'label':LABEL,'confidence':CONFIDENCE_READABLE}
        out_df = out_df.append(new_row, ignore_index=True)

    print("\n")
    return out_df.to_csv(index=False)
