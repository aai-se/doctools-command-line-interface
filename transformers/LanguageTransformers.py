import json
import pandas as pd

def GetListAsCsv(jsonResults):
    #{'page': {'offset': 0, 'total': 1, 'totalFilter': 1}, 'list': [{'id': '1', 'type': 'ATTENDED_BOT_RUNNER', 'hostName': 'EC2AMAZ-4PFQG1I', 'userId': '', 'userName': '', 'status': 'CONNECTED',
    # 'poolName': '', 'fullyQualifiedHostName': '-', 'updatedBy': 'iqbot', 'updatedOn': '2019-11-18T05:35:39.154Z'}]}

    out_df = pd.DataFrame(columns=['ID','LANGUAGE'])

    itemList = jsonResults['result']['response']

    for item in itemList:
        ID = item
        LANG = itemList[item]

        new_row = {'ID':ID,'LANGUAGE':LANG}
        out_df = out_df.append(new_row, ignore_index=True)

    return out_df.to_csv(index=False)
