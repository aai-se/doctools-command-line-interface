##############################################################
#
# Author: Bren Sapience
# Date: Dec 2019
# Scope: Provide CLI functions around Doctools
#
# Call Examples:
#
#   python3 ./
#   python3 ./
#
##############################################################

import argparse
import sys
sys.path.insert(1, './logics')
sys.path.insert(1, './libs')
import ClassifiersLogics
import AllInOneLogics
import LanguageLogics
import DataUtils

#Allowed Operations - should be lower case
SupportedOperations = ['list','create','delete','list_labels','add_labels','add_docs','train','classify','auto']


    # perform_validation - Whether to perform validation on the trained model	Boolean - Default: True
    # folds_count - The number of folds to do as part of cross-validation	Integer - Default:	3
    # repeats_count - The number of times to run cross-validation	Integer	- Default: 1
    # averaging_method - How to calculate the results across repeats	AM_Macro | AM_Micro	- Default: AM_Macro


CLASSIFIER_TYPES = ["CT_Combined","CT_Text","CT_Image"] #Text, Image or Combined Classifier
DEFAULT_CLASSIFIER_TYPE = CLASSIFIER_TYPES[0]

TRAINING_MODES = ["TM_Balanced","TM_Recall","TM_Precision"] #What to prioritize during Training
DEFAULT_TRAINING_MODE = TRAINING_MODES[0]

DEFAULT_PERFORM_VALIDATION = True
DEFAULT_FOLDS_COUNT = 3
DEFAULT_REPEAT_COUNT = 1

AVERAGING_METHODS = ["AM_Macro","AM_Micro"]
DEFAULT_AVERAGING_MTD = AVERAGING_METHODS[0]

def checkParameter(Value,ValueOptions):
    if not Value in ValueOptions:
        print("\t Error: "+ Value +" is not a valid option. Possible Values: "+str(ValueOptions))
        exit(1)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Operation should be available in all modules
    parser.add_argument("-o","--operation", type=str,default = "list", help = "<list,create,delete,list_labels,add_labels,add_docs,train,classify,auto>: classifiers", dest = "OPERATION")
    parser.add_argument("-c","--classifier", type=str,default = "", help = "Classifier ID", dest = "CLASSIFIERID")
    parser.add_argument("-l","--labels", type=str,default = "", help = "Add Labels to classifier (CSV list)", dest = "LABELS")
    parser.add_argument("-d","--docids", type=str,default = "", help = "Add Document IDs to add to classifier (CSV)", dest = "DOCIDS")

    parser.add_argument("-m","--magic", type=str,default = "", help = "Root Folder with Labeled Files to Use", dest = "MAGICFOLDER")
    parser.add_argument("-g","--languages", type=str,default = "English", help = "Comma Seperated List of 3-char Languages (ex: \"eng,fra\")", dest = "LANGUAGES")

    parser.add_argument("-c_type","--c_type", type=str,default = DEFAULT_CLASSIFIER_TYPE, help = "<optional> Classifier Type - "+str(CLASSIFIER_TYPES), dest = "CLASSIFIER_TYPE")
    parser.add_argument("-c_mode","--c_mode", type=str,default = DEFAULT_TRAINING_MODE, help = "<optional> Classifier Mode"+str(TRAINING_MODES), dest = "CLASSIFIER_MODE")
    parser.add_argument("-c_validation","--c_validation", type=bool,default = DEFAULT_PERFORM_VALIDATION, help = "<optional> Perform Validation while training", dest = "PERFORM_VALIDATION")
    parser.add_argument("-c_folds","--c_folds", type=int,default = DEFAULT_FOLDS_COUNT, help = "<optional> Fold Counts", dest = "FOLD_COUNT")
    parser.add_argument("-c_repeats","--c_repeats", type=int,default = DEFAULT_REPEAT_COUNT, help = "<optional> Repeat Count", dest = "REPEAT_COUNT")
    parser.add_argument("-c_avgmethod","--c_avgmethod", type=str,default = DEFAULT_AVERAGING_MTD, help = "<optional> Averaging Method"+str(AVERAGING_METHODS), dest = "AVERAGING_METHOD")

    OPTIONS = parser.parse_args()

    #Check that operation is Supported
    if OPTIONS.OPERATION.lower() not in SupportedOperations:
        parser.error('Operation passed (-o) is not supported: '+OPTIONS.OPERATION)

    DataUtils.DisplayURLandPort()

    ###
    # list operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[0]: #list
        ClassifiersLogics.list()

    ###
    # create operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[1]: #create
        ClassifiersLogics.create()

    ###
    # delete operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[2]: #delete
        if not OPTIONS.CLASSIFIERID:
            parser.error('You need to pass parameter -c or (--classifier)')

        ClassifiersLogics.delete(OPTIONS.CLASSIFIERID)

    ###
    # add label operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[4]: #delete
        if not OPTIONS.CLASSIFIERID:
            parser.error('You need to pass parameter -c or (--classifier)')

        if not OPTIONS.LABELS:
            parser.error('You need to pass parameter -l or (--labels)')

        ClassifiersLogics.add_labels(OPTIONS.CLASSIFIERID,OPTIONS.LABELS)

    ###
    # list label operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[3]: #delete
        if not OPTIONS.CLASSIFIERID:
            parser.error('You need to pass parameter -c or (--classifier)')

        ClassifiersLogics.list_labels(OPTIONS.CLASSIFIERID)

    ###
    # add docs operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[5]: #delete
        if not OPTIONS.CLASSIFIERID:
            parser.error('You need to pass parameter -c or (--classifier)')

        if not OPTIONS.DOCIDS:
            parser.error('You need to pass parameter -d or (--docids)')

        if not OPTIONS.LABELS:
            parser.error('You need to pass parameter -l or (--labels)')

        Labels = OPTIONS.LABELS.split(",")
        if len(Labels) > 1:
            parser.error('Too many Labels passed for Operation add_docs. Only 1 Label should be passed')

        ClassifiersLogics.add_docs(OPTIONS.CLASSIFIERID,OPTIONS.DOCIDS,OPTIONS.LABELS)

    ###
    # train operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[6]: #train
        if not OPTIONS.CLASSIFIERID:
            parser.error('You need to pass parameter -c or (--classifier)')

        checkParameter(OPTIONS.CLASSIFIER_TYPE,CLASSIFIER_TYPES)
        checkParameter(OPTIONS.CLASSIFIER_MODE,TRAINING_MODES)
        checkParameter(OPTIONS.AVERAGING_METHOD,AVERAGING_METHODS)

        ClassifiersLogics.train(OPTIONS.CLASSIFIERID,
            OPTIONS.CLASSIFIER_TYPE,
            OPTIONS.CLASSIFIER_MODE,
            OPTIONS.PERFORM_VALIDATION,
            OPTIONS.FOLD_COUNT,
            OPTIONS.REPEAT_COUNT,
            OPTIONS.AVERAGING_METHOD
        )

    ###
    # classify operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[7]: #delete
        if not OPTIONS.CLASSIFIERID:
            parser.error('You need to pass parameter -c or (--classifier)')

        if not OPTIONS.DOCIDS:
            parser.error('You need to pass parameter -d or (--docids)')

        ClassifiersLogics.classify(OPTIONS.CLASSIFIERID,OPTIONS.DOCIDS)

    ###
    # Auto
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[8]: #train
        if not OPTIONS.MAGICFOLDER:
            parser.error('You need to pass parameter -m or (--magic)')

        checkParameter(OPTIONS.CLASSIFIER_TYPE,CLASSIFIER_TYPES)
        checkParameter(OPTIONS.CLASSIFIER_MODE,TRAINING_MODES)
        checkParameter(OPTIONS.AVERAGING_METHOD,AVERAGING_METHODS)

        LanguagesAllSupported,ProblematicLanguages = LanguageLogics.checkLanguages(OPTIONS.LANGUAGES)
        if not LanguagesAllSupported:
            print("!! Fatal Error: Languages Unsupported: "+str(ProblematicLanguages))
            exit(1)
        AllInOneLogics.CreateAndTrainModel(
            OPTIONS.MAGICFOLDER,
            OPTIONS.LANGUAGES,
            OPTIONS.CLASSIFIER_TYPE,
            OPTIONS.CLASSIFIER_MODE,
            OPTIONS.PERFORM_VALIDATION,
            OPTIONS.FOLD_COUNT,
            OPTIONS.REPEAT_COUNT,
            OPTIONS.AVERAGING_METHOD
        )
