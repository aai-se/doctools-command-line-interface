import os

ListOfFiles = "./Doctools_classifiers.py,./Doctools_languages.py"
FolderPath = "./docs/IRS-Sample-Forms"

def getFolderList(FolderPath):
    Labels = []
    FileListFromFolder = []
    for (dirpath, dirnames, filenames) in os.walk(FolderPath):
        for folder in dirnames:
            Labels.append(folder)
            file = os.path.join(dirpath,folder)
            FullPath = os.path.abspath(file)
            FileListFromFolder.append(FullPath)

    return FileListFromFolder,Labels

A,B = getFolderList(FolderPath)
print(A)
print(B)
