import requests
import json
import sys
import os

sys.path.insert(1, './libs')
sys.path.insert(1, './transformers')
import DataUtils
import StdResponses
import ClassifiersTransformers


def Process_ocr_Response(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if ExitScriptOnSuccess:
            exit(1)
        else:
            result = json.loads(res.text)
            return result['result']['response'],True
    else:
        result = json.loads(res.text)
        if(ExitScriptOnSuccess):
            print(result['result']['response'])
            exit(0)

        else:
            return result['result']['response'],False
