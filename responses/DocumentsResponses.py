import requests
import json
import sys
import os

sys.path.insert(1, './libs')
sys.path.insert(1, './transformers')
import DataUtils
import StdResponses
import ClassifiersTransformers


def Process_List_Label_Response(res,CsvOutput):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            print(result['result']['response'])
            exit(0)
        else:
            #print(result)
            print(result['result']['response'])
            exit(0)


def Process_list_Response(res,CsvOutput):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)

        if isCsvOutput:
            print(ClassifiersTransformers.GetListAsCsv(result))
            exit(0)
        else:
            #print(result)
            print(result['result']['response'])
            exit(0)

def Process_upload_Response(file,res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if(ExitScriptOnSuccess):
            exit(1)
        else:
            print("Error")
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            if(ExitScriptOnSuccess):
                print(file+","+result['result']['response'])
                exit(0)
            else:
                return result['result']['response'],file
        else:
            #print(result)

            if(ExitScriptOnSuccess):
                print(file+","+result['result']['response'])
                exit(0)
            else:
                return result['result']['response'],file

def Process_Get_Info(res,CsvOutput=False,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if ExitScriptOnSuccess:
            print("Error.")
            exit(1)
        else:
            return None,True
    else:
        result = json.loads(res.text)
        if(ExitScriptOnSuccess):
            print(result['result']['response'])
            exit(0)
        else:
            return result['result']['response'],False

def Process_std_Response(res,CsvOutput):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            print("ok.")
            #exit(0)
        else:
            #print(result)
            print("ok.")
            #exit(0)


def Process_delete_Response2(res):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,False)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)
        print("\t (INFO: Document Clean Up Done.)")

def Process_delete_Response(res,CsvOutput):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            print(result['result']['response'])
            #exit(0)
        else:
            #print(result)
            print(result['result']['response'])
            #exit(0)
