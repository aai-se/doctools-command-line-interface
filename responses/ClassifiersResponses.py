import requests
import json
import sys
import os
import shutil

sys.path.insert(1, './libs')
sys.path.insert(1, './transformers')
import DataUtils
import StdResponses
import ClassifiersTransformers

def Process_Add_Docs_Response(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if ExitScriptOnSuccess:
            exit(1)
        else:
            #print("Error.")
            return True
    else:
        if(ExitScriptOnSuccess):
            exit(0)
        else:
            return False

def Process_Train_Response_CLI(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if ExitScriptOnSuccess:
            exit(1)
        else:
            result = json.loads(res.text)
            return result['result']['response'],True
    else:
        result = json.loads(res.text)
        if(ExitScriptOnSuccess):
            print(result['result']['response'])
            exit(0)

        else:
            return result['result']['response'],False



def Process_Classify_Response(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if ExitScriptOnSuccess:
            print("Error.")
            exit(1)
        else:
            return None,True
    else:
        result = json.loads(res.text)
        if(ExitScriptOnSuccess):
            print(result['result']['response'])
            exit(0)
        else:
            return result['result']['response'],False

def Process_Import_Response(res):
    ExitScriptOnSuccess=False
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,False)
    if(isError):
        if ExitScriptOnSuccess:
            exit(1)
        #else:
            #print("Error.")
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            ID = result['result']['response']
            print(result['status']+": "+ID)
            if(ExitScriptOnSuccess):
                exit(0)
        else:
            #print(result)
            ID = result['result']['response']
            print(result['status']+": "+ID)
            if(ExitScriptOnSuccess):
                exit(0)

def Process_List_Label_Response(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if ExitScriptOnSuccess:
            exit(1)
        else:
            print("Error.")
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            print(result['result']['response'])
            if(ExitScriptOnSuccess):
                exit(0)
        else:
            #print(result)
            print(result['result']['response'])
            if(ExitScriptOnSuccess):
                exit(0)

def Process_listInternal_Response(res,CsvOutput):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)
        return ClassifiersTransformers.GetListAsList(result)


def Process_list_Response(res,CsvOutput):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            print(ClassifiersTransformers.GetListAsCsv(result))
            exit(0)
        else:
            #print(result)
            print(result['result']['response'])
            exit(0)

def Process_create_Response(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if(ExitScriptOnSuccess):
            exit(1)
        else:
            print("Error")
            return None
    else:
        result = json.loads(res.text)
        if isCsvOutput:

            if(ExitScriptOnSuccess):
                print(result['result']['response'])
                exit(0)
            else:
                return result['result']['response']
        else:
            if(ExitScriptOnSuccess):
                print(result['result']['response'])
                exit(0)
            else:
                return result['result']['response']

def Process_std_Response(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if(ExitScriptOnSuccess):
            exit(1)
        else:
            return True
    else:
        result = json.loads(res.text)
        if not ExitScriptOnSuccess:
            return False
        if isCsvOutput:
            print("\t\tok.")
            #exit(0)
        else:
            if not ExitScriptOnSuccess:
                return False
            #print(result)
            print("\t\tok.")
            #exit(0)

def Process_delete_Response(res,CsvOutput,ExitScriptOnSuccess=False):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        if ExitScriptOnSuccess:
            exit(1)
        else:
            return None,True
    else:
        result = json.loads(res.text)
        if isCsvOutput:

            if ExitScriptOnSuccess:
                print(result['result']['response'])
                exit(0)
            else:
                return result['result']['response'], False
        else:
            #print(result)

            if ExitScriptOnSuccess:
                print(result['result']['response'])
                exit(0)
            else:
                return result['result']['response'], False
