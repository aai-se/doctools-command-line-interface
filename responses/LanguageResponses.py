import requests
import json
import sys
import os

sys.path.insert(1, './libs')
sys.path.insert(1, './transformers')
import DataUtils
import StdResponses
import LanguageTransformers

def Process_list_Response(res,CsvOutput):
    isError,isCsvOutput = StdResponses.ProcessStdResponse(res,CsvOutput)
    if(isError):
        exit(1)
    else:
        result = json.loads(res.text)
        if isCsvOutput:
            print(LanguageTransformers.GetListAsCsv(result))
            exit(0)
        else:
            #print(result)
            print(result['result']['response'])
            exit(0)
