import os
import random
import json

DATAFILEPATH = "./libs/"
CONFIGFILENAME = "config.json"
CONFIGFILE = os.path.join(DATAFILEPATH, CONFIGFILENAME)

def GetEnclosingPathAndFilename(full_filename):
    fname = os.path.basename(full_filename)
    onedir = os.path.join(os.path.basename(os.path.dirname(full_filename)), os.path.basename(full_filename))
    return onedir

def DisplayURLandPort():
    BackendUrl = GetUrl()
    print("INFO - Backend URL:" + BackendUrl)
    return None

def GetUrl():
    with open(CONFIGFILE) as json_file:
        data = json.load(json_file)
        url = data['URL']
        port = data['PORT']
        URL = url+":"+str(port)
        return URL

def getFolderListAndLabels(FolderPath):
    Labels = []
    FileListFromFolder = []
    for (dirpath, dirnames, filenames) in os.walk(FolderPath):
        for folder in dirnames:
            LabelName = folder.upper()
            Labels.append(LabelName)
            file = os.path.join(dirpath,folder)
            FullPath = os.path.abspath(file)
            FileListFromFolder.append(FullPath)

    return FileListFromFolder,Labels

def getFileListFromFolders(FolderName):
    FileListFromFolder = []
    for (dirpath, dirnames, filenames) in os.walk(FolderName):
        for filename in filenames:
            if not filename.startswith("."):
                file = os.path.join(dirpath,filename)
                FullPath = os.path.abspath(file)
                FileListFromFolder.append(FullPath)
    return FileListFromFolder

def getFileList(ListOfFiles):
    RawFileList = ListOfFiles.split(",")

    FileList = []
    for file in RawFileList:
        if not file.startswith("."):
            FilePath = os.path.abspath(file)
            FileList.append(FilePath)
        # Super dirty..
        if file.startswith("./") or file.startswith(".\\"):
            FilePath = os.path.abspath(file)
            FileList.append(FilePath)

    return FileList

def getListOfItems(ListOfThings):

    if(type(ListOfThings) == type([])):
        return ListOfThings

    SplitList = ListOfThings.split(",");
    FinalList = []
    for i in SplitList:
        filtered = i.replace("[","").replace("]","").replace("'","")#.replace(" ","")
        FinalList.append(filtered)

    return FinalList
