import requests
import json
import sys
import os
import urllib.parse

sys.path.insert(1, './libs')
sys.path.insert(1, './responses')
import DataUtils
import DocumentsResponses
from pathlib import Path

DOC_LIST_URI = "/documents"
DOC_LIST_REQ_TYPE = "GET"

DOC_UPLOAD_URI = "/documents"
DOC_UPLOAD_REQ_TYPE = "POST"

CLASS_GET_INFO_REQ_TYPE = "GET"
def get_CLASS_GET_INFO_URI(DocID):
    #IQBot/api/projects/4a505399-e0d5-45b4-ac18-ac88a1d9763a/categories/8/bots/8c2f9553-6c04-40da-aa45-2116716d3f38/state
    return "/documents/"+DocID

DOC_DELETE_REQ_TYPE = "DELETE"
def get_DOC_DELETE_URI(DocID):
    #IQBot/api/projects/4a505399-e0d5-45b4-ac18-ac88a1d9763a/categories/8/bots/8c2f9553-6c04-40da-aa45-2116716d3f38/state
    return "/documents/"+DocID


def get_DOC_UPLOAD_BODY(fullpathandfilename):
    # Fix: remove escape character
    image_filename = os.path.basename(fullpathandfilename)
    dirname = os.path.dirname(fullpathandfilename)
    image_filename = image_filename.replace("\\","")

    formattedpath = os.path.join(dirname,image_filename)
    multipart_form_data = {
        'file': (image_filename, open(formattedpath, 'rb'))
    }

    return multipart_form_data


def list(CsvOutput=False):
    URL = urllib.parse.urljoin(DataUtils.GetUrl(), DOC_LIST_URI)

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }
    response = requests.request(DOC_LIST_REQ_TYPE, URL, headers=headers)
    isInError = DocumentsResponses.Process_list_Response(response,CsvOutput)

def upload(FilePaths,FolderPath,CsvOutput=False,ExistScript=False):
    FileList = []
    if(FolderPath):
        FileList = DataUtils.getFileListFromFolders(FolderPath)
    if(FilePaths):
        FileList = DataUtils.getFileList(FilePaths)

    AllDocs = []

    for file in FileList:
        #print(file)
        multipart = get_DOC_UPLOAD_BODY(file)

        URL = urllib.parse.urljoin(DataUtils.GetUrl(), DOC_UPLOAD_URI)

        response = requests.request(DOC_UPLOAD_REQ_TYPE, URL,files=multipart)
        (id,doc) = DocumentsResponses.Process_upload_Response(file,response,CsvOutput,ExistScript)
        info = tuple((id,doc))
        #print("Uploaded Document: "+doc+" With ID: " + id)

        AllDocs.append(info)

    return AllDocs

def get_doc_info(DocID):

    URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_CLASS_GET_INFO_URI(DocID))
    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(CLASS_GET_INFO_REQ_TYPE, URL, headers=headers)
    Res,isInError = DocumentsResponses.Process_Get_Info(response,False,False)
    #print(Res['contentType'])
    return Res,isInError

def delete(DocIDs,CsvOutput=False,AltOutput=False):
    IDs = DataUtils.getListOfItems(DocIDs)
    for id in IDs:
        URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_DOC_DELETE_URI(id))

        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache"
            }

        response = requests.request(DOC_DELETE_REQ_TYPE, URL, headers=headers)
        #print(response)
        if AltOutput:
            isInError = DocumentsResponses.Process_delete_Response2(response)
        else:
            isInError = DocumentsResponses.Process_delete_Response(response,CsvOutput)
