import requests
import json
import sys
import os
import urllib.parse

sys.path.insert(1, './libs')
sys.path.insert(1, './responses')
import DataUtils
import LanguageResponses

DEVICE_LIST_URI = "/languages"
DEVICE_LIST_REQ_TYPE = "GET"


def list(CsvOutput):
    URL = urllib.parse.urljoin(DataUtils.GetUrl(), DEVICE_LIST_URI)

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(DEVICE_LIST_REQ_TYPE, URL, headers=headers)
    isInError = LanguageResponses.Process_list_Response(response,CsvOutput)


def get_list():
    URL = urllib.parse.urljoin(DataUtils.GetUrl(), DEVICE_LIST_URI)

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(DEVICE_LIST_REQ_TYPE, URL, headers=headers)
    result = json.loads(response.text)
    return result['result']['response']


def checkLanguages(Languages):
    LangList = Languages.split(",")
    ProblematicLangs = []
    SupportedLangList = get_list()

    #print(SupportedLangList)
    #print(LangList)
    Res = True
    for lang in LangList:
        if not lang in SupportedLangList:
            ProblematicLangs.append(lang)
            Res = False

    #print("Status:"+str(Res))
    #print("Lang:"+str(ProblematicLangs))
    return Res,ProblematicLangs
