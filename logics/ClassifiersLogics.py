import requests
import json
import sys
import os
import urllib.parse
import re


sys.path.insert(1, './logics')
sys.path.insert(1, './transformers')
sys.path.insert(1, './responses')
sys.path.insert(1, './libs')
import DataUtils
import ClassifiersResponses
import StdResponses
import DocumentsLogics
import OCRLogics
import ClassifiersTransformers

CLASS_LIST_URI = "/classifiers"
CLASS_LIST_REQ_TYPE = "GET"

CLASS_CREATE_URI = "/classifiers"
CLASS_CREATE_REQ_TYPE = "POST"

CLASS_EXPORT_REQ_TYPE = "GET"
def get_CLASS_EXPORT_URI(ClassifierID):
    #IQBot/api/projects/4a505399-e0d5-45b4-ac18-ac88a1d9763a/categories/8/bots/8c2f9553-6c04-40da-aa45-2116716d3f38/state
    return "/classifiers/"+ClassifierID+"/export"

CLASS_IMPORT_REQ_TYPE = "POST"
CLASS_IMPORT_REQ_TYPEURI = "/classifiers/import"
def get_CLASS_IMPORT_BODY(OVERWRITE_FLAG):
    b = json.dumps(
    {
        "overwrite": OVERWRITE_FLAG
    }
    )
    return b

CLASS_DELETE_REQ_TYPE = "DELETE"
def get_CLASS_DELETE_URI(ClassifierID):
    #IQBot/api/projects/4a505399-e0d5-45b4-ac18-ac88a1d9763a/categories/8/bots/8c2f9553-6c04-40da-aa45-2116716d3f38/state
    return "/classifiers/"+ClassifierID

CLASS_ADD_LABEL_REQ_TYPE = "POST"
def get_CLASS_ADD_LABEL_URI(ClassifierID,label):
    #IQBot/api/projects/4a505399-e0d5-45b4-ac18-ac88a1d9763a/categories/8/bots/8c2f9553-6c04-40da-aa45-2116716d3f38/state

    URL = "/classifiers/"+ClassifierID+"/labels?label="+label
    #print("WEgfsdfg: "+URL)
    return URL


CLASS_CLASSIFY_REQ_TYPE = "POST"
def get_CLASS_CLASSIFY_URI(ClassifierID,DocID):
    #/classifiers/0e87c9c5-2298-4c69-8018-34d81cf8d8cf/classify?document=ba658fbf-9ff3-44fb-aa0a-7f54f5e45976
    URL = "/classifiers/"+ClassifierID+"/classify?document="+DocID
    return URL

CLASS_TRAIN_REQ_TYPE = "POST"
def get_CLASS_TRAIN_URI(ClassifierID):
    #/classifiers/0e87c9c5-2298-4c69-8018-34d81cf8d8cf/train
    URL = "/classifiers/"+ClassifierID+"/train"
    return URL

def get_CLASS_TRAIN_BODY(CLASSIFIER_TYPE,CLASSIFIER_MODE,PERFORM_VALIDATION,FOLD_COUNT,REPEAT_COUNT,AVERAGING_METHOD):
    b = json.dumps(
    {
        "classifierType": CLASSIFIER_TYPE,
        "trainingMode":CLASSIFIER_MODE,
        "performValidation":PERFORM_VALIDATION,
        "foldsCount":FOLD_COUNT,
        "repeatsCount":REPEAT_COUNT,
        "averagingMethod":AVERAGING_METHOD
    }
    )
    #print(b)
    return b

CLASS_ADD_DOC_REQ_TYPE = "POST"
def get_CLASS_ADD_DOC_URI(ClassifierID,DocID,Label):
    #classifiers/94000c8c-3869-4a87-88bd-46c352a7ae21/labels/creditmemos/documents
    URL = "/classifiers/"+ClassifierID+"/labels/"+Label+"/documents"
    PAYLOAD = "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"document\"\r\n\r\n"+DocID+"\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"pageNumber\"\r\n\r\n1\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--"
    HEADERS = {
    'content-type': "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW",
    'cache-control': "no-cache"
    }

    return URL,PAYLOAD,HEADERS


CLASS_LIST_LABEL_REQ_TYPE = "GET"
def get_CLASS_LIST_LABEL_URI(ClassifierID):
    #IQBot/api/projects/4a505399-e0d5-45b4-ac18-ac88a1d9763a/categories/8/bots/8c2f9553-6c04-40da-aa45-2116716d3f38/state
    return "/classifiers/"+ClassifierID+"/labels"


def export(ClassifierIDs,OutputPath):
    ClassIDList = DataUtils.getListOfItems(ClassifierIDs)
    for id in ClassIDList:

        URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_CLASS_EXPORT_URI(id))

        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        chunk_size=128
        OutputFile = os.path.join(OutputPath,id+".icem")
        r0 = r1 = requests.get(URL, stream=True)

        IsRespOK = StdResponses.ProcessStdEmptyResponse(r0)

        if not IsRespOK:
            print("Error: Could Not Export Classifier with id: "+id)

        else:
            with open(OutputFile, 'wb') as fd:
                for chunk in r1.iter_content(chunk_size=chunk_size):
                    fd.write(chunk)
            print("Success: Exported Classifier: "+OutputFile)


def importClassifier(InputFile,OverwriteFlag):

    URL = urllib.parse.urljoin(DataUtils.GetUrl(), CLASS_IMPORT_REQ_TYPEURI)
    headers = {}
    payload = {'overwrite':OverwriteFlag}#get_CLASS_IMPORT_BODY(0)
    files = {'file': open(InputFile,'rb')}

    response = requests.post(URL, headers=headers, files=files, data=payload)
    #response = requests.request("POST", URL, headers=headers, data = payload, files = files)
    ClassifiersResponses.Process_Import_Response(response)
    #print(response.text.encode('utf8'))



def listInternal(CsvOutput=False):
    URL = urllib.parse.urljoin(DataUtils.GetUrl(), CLASS_LIST_URI)

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(CLASS_LIST_REQ_TYPE, URL, headers=headers)
    allClassifiers = ClassifiersResponses.Process_listInternal_Response(response,CsvOutput)
    return allClassifiers

def list(CsvOutput=False):
    URL = urllib.parse.urljoin(DataUtils.GetUrl(), CLASS_LIST_URI)

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(CLASS_LIST_REQ_TYPE, URL, headers=headers)
    isInError = ClassifiersResponses.Process_list_Response(response,CsvOutput)

def create(CsvOutput=False,ExitScript=True):
    URL = urllib.parse.urljoin(DataUtils.GetUrl(), CLASS_CREATE_URI)

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(CLASS_CREATE_REQ_TYPE, URL, headers=headers)
    id = ClassifiersResponses.Process_create_Response(response,CsvOutput,ExitScript)
    return id

def delete(ClassifierIDs,CsvOutput=False):
    ClassIDList = DataUtils.getListOfItems(ClassifierIDs)
    for id in ClassIDList:

        URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_CLASS_DELETE_URI(id))

        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = requests.request(CLASS_DELETE_REQ_TYPE, URL, headers=headers)
        res,isInError = ClassifiersResponses.Process_delete_Response(response,CsvOutput,False)
        if(not isInError):
            print("\t OK: Classifier Deleted: "+id)
        else:
            print("\t ! Error: Could Not Delete Classifier: "+id)

def add_labels(ClassifierID,Labels,CsvOutput=False,ExitScript=True):
    LabelList = DataUtils.getListOfItems(Labels)
    for label in LabelList:
        #print("adding label: "+label+" for classifier: "+ClassifierID)

        URL = urllib.parse.urljoin(DataUtils.GetUrl(),get_CLASS_ADD_LABEL_URI(ClassifierID,label))
        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache"
        }

        response = requests.request(CLASS_ADD_LABEL_REQ_TYPE, URL, headers=headers)
        isInError = ClassifiersResponses.Process_std_Response(response,CsvOutput,ExitScript)

def add_label(ClassifierID,Label,CsvOutput=False,ExitScript=True):

    URL = urllib.parse.urljoin(DataUtils.GetUrl(),get_CLASS_ADD_LABEL_URI(ClassifierID,Label))
    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(CLASS_ADD_LABEL_REQ_TYPE, URL, headers=headers)
    isInError = ClassifiersResponses.Process_std_Response(response,CsvOutput,False)
    return isInError

def add_doc(ClassifierID,DocID,Label,CsvOutput=False,ExistScript=True):

    short_url,PAYLOAD,headers = get_CLASS_ADD_DOC_URI(ClassifierID,DocID,Label)
    URL = urllib.parse.urljoin(DataUtils.GetUrl(), short_url)
    response = requests.request(CLASS_ADD_LABEL_REQ_TYPE, URL,data=PAYLOAD, headers=headers)
    isInError = ClassifiersResponses.Process_Add_Docs_Response(response,CsvOutput,ExistScript)
    return isInError

def add_docs(ClassifierID,DocIDs,Label,CsvOutput=False,ExistScript=True):
    DocIdList = DataUtils.getListOfItems(DocIDs)
    for id in DocIdList:

        short_url,PAYLOAD,headers = get_CLASS_ADD_DOC_URI(ClassifierID,id,Label)
        URL = urllib.parse.urljoin(DataUtils.GetUrl(), short_url)
        #print(URL)
        response = requests.request(CLASS_ADD_LABEL_REQ_TYPE, URL,data=PAYLOAD, headers=headers)
        isInError = ClassifiersResponses.Process_Add_Docs_Response(response,CsvOutput,ExistScript)

def list_labels(ClassifierID,CsvOutput=False):

    URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_CLASS_LIST_LABEL_URI(ClassifierID))

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(CLASS_LIST_LABEL_REQ_TYPE, URL, headers=headers)
    isInError = ClassifiersResponses.Process_List_Label_Response(response,CsvOutput)

def train(ClassifierID,CLASSIFIER_TYPE,CLASSIFIER_MODE,PERFORM_VALIDATION,FOLD_COUNT,REPEAT_COUNT,AVERAGING_METHOD,CsvOutput=False):

    URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_CLASS_TRAIN_URI(ClassifierID))
    payload = get_CLASS_TRAIN_BODY(CLASSIFIER_TYPE,CLASSIFIER_MODE,PERFORM_VALIDATION,FOLD_COUNT,REPEAT_COUNT,AVERAGING_METHOD)
    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache"
    }

    response = requests.request(CLASS_TRAIN_REQ_TYPE, URL,data=payload, headers=headers)
    Res,isInError = ClassifiersResponses.Process_Train_Response_CLI(response,CsvOutput)
    return Res,isInError


def classify(ClassifierID,DocIDS,CsvOutput=False):
    DocIdList = DataUtils.getListOfItems(DocIDS)
    for doc in DocIdList:
        # Check if the document is an ID or a file path
        match = re.search("[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{8}", doc)
        if(not match):
            print("\t => Document passed looks like a file.. Uploading it first:"+doc)
            DocInfo = DocumentsLogics.upload(doc,None,False,False)
            doc,filepath = DocInfo[0]
            print("\t => Document ID: "+doc)

        # Forcing OCR of Document in order to process any type of docs
        Res,IsErr = DocumentsLogics.get_doc_info(doc)
        if IsErr:
            print("Error.")
            exit(1)
        else:
            ID,IsErr = OCRLogics.ocr(doc)
            if(IsErr):
                print("Error while OCRing the document.")
                exit(1)
            else:
                doc=ID
                #print("\t => INFO: OCR Done.")
            '''
            ContentType = Res['contentType']
            if(ContentType != "application/pdf"):
                print("\t => INFO: File needs OCR.")
                ID,IsErr = OCRLogics.ocr(doc)
                if(IsErr):
                    print("Error while OCRing the document.")
                    exit(1)
                else:
                    doc=ID
                    print("\t => INFO: OCR Done.")
            '''
        URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_CLASS_CLASSIFY_URI(ClassifierID,doc))
        headers = {
            'Content-Type': "application/json",
            'cache-control': "no-cache"
            }

        response = requests.request(CLASS_CLASSIFY_REQ_TYPE, URL, headers=headers)
        Res,isInError = ClassifiersResponses.Process_Classify_Response(response,CsvOutput)
        if isInError:
            print("\t Error. Could not Classify Document: "+doc)
            exit(1)
        else:
            print("\n\t Results for document: "+doc)
            ClassifiersTransformers.GetClassifyResponseAsText(Res)
