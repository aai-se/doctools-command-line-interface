import requests
import json
import sys
import os
import urllib.parse

sys.path.insert(1, './libs')
sys.path.insert(1, './logics')
sys.path.insert(1, './transformers')
sys.path.insert(1, './responses')
import DataUtils
import ClassifiersResponses
import AllInOneLogics
import ClassifiersLogics
import DocumentsLogics
import OCRLogics
import ClassifiersTransformers

def CreateAndTrainModel(RootFolder,LANGUAGES,CLASSIFIER_TYPE,CLASSIFIER_MODE,PERFORM_VALIDATION,FOLD_COUNT,REPEAT_COUNT,AVERAGING_METHOD):
    FolderList,Labels = DataUtils.getFolderListAndLabels(RootFolder)
    FoldersAndLabels = zip(FolderList,Labels)

    # creating Classifier
    res = ClassifiersLogics.create(False,False)
    ClassifierID = str(res)
    print("\nClassifier ID Generated: ["+ClassifierID+"]")


    # Adding Labels to Classifier
    print("\t => Adding Labels: "+str(Labels))

    for label in Labels:
        Err = ClassifiersLogics.add_label(ClassifierID,label,False,False)
        if Err:
            print("\t !! ERROR: Could not add label: "+label)
            exit(1)
        else:
            print("\t => Label "+label+" added successfully.")


    # Upload Documents to Doctools and attaching them to Classifier

    print("\n=> Uploading Files to Classifier:")
    for (folder,label) in FoldersAndLabels:
        print("\n\t => Uploading all Files in Folder: "+folder)
        FileList = DataUtils.getFileListFromFolders(folder)
        LangList = LANGUAGES.split(",")
        for file in FileList:
            #print("\t => Uploading Document: "+file)
            DOC = DocumentsLogics.upload(file,None,False,False)
            (id,doc) = DOC[0]
            try:
                ID,IsErr = OCRLogics.ocr(id,LangList)
                if IsErr:
                    print("\tINFO: OCR Step Failed:"+id)

                enclosing = DataUtils.GetEnclosingPathAndFilename(doc)
                Err = ClassifiersLogics.add_doc(ClassifierID,id,label,False,False)
                if Err:
                    print("\t !! ERROR: Could not add document: "+id+" - SKIPPING.")
                    #exit(1)
                else:
                    print("\t => Document added to Label: ["+label+"] - "+str(id)+ " ("+enclosing+")")
                    #DocumentsLogics.delete(id,False,True)
                    # Should remove uploaded doc from Doctools?
            except Exception as e:
                #the DOC needs to be removed so as not to corrupt the classifier
                DocumentsLogics.delete(id)
                print("Error Processing File:"+file+" [Deleting from Backend & Skipping]")
                continue

    # Start Training
    print("\n=> Training Classifier:")
    Res,Err = ClassifiersLogics.train(ClassifierID,CLASSIFIER_TYPE,CLASSIFIER_MODE,PERFORM_VALIDATION,FOLD_COUNT,REPEAT_COUNT,AVERAGING_METHOD,False)
    if(Err):
        print("\t !! ERROR: Could train model: "+str(Res))
    else:
        print("\t => Model Training Done. Model ID: "+ClassifierID)
        ClassifiersTransformers.GetTrainResponseAsText(Res)
