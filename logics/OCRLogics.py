import requests
import json
import sys
import os
import urllib.parse
import re

sys.path.insert(1, './libs')
sys.path.insert(1, './logics')
sys.path.insert(1, './transformers')
sys.path.insert(1, './responses')
import DataUtils
import OCRResponses
import DocumentsLogics
import ClassifiersTransformers

CLASS_OCR_REQ_TYPE = "POST"
def get_CLASS_OCR_URI(docID):
    return "/documents/"+docID+"/ocr"

def GET_OCR_BODY(languages):

    list_of_languages = ''
    for l in languages:
        if list_of_languages == "":
            list_of_languages = list_of_languages + '"'+l+'"'
        else:
            list_of_languages = list_of_languages + ',"'+l+'"'

    payload = {'languages':'['+list_of_languages+']','settings':'{"deskewImage": true}'}
    payload = {'languages':'['+list_of_languages+']'}
    return payload
'''
def GET_OCR_BODY(lang):

    payload = {'languages':'["'+lang+'"]','settings':'{"deskewImage": true}'}
    payload = {'languages':'["'+lang+'"]'}
    return payload
'''
def ocr_docs(DocIDs,Languages=["eng"],CsvOutput=False):
    IDs = DataUtils.getListOfItems(DocIDs)
    for id in IDs:
        Res,isInError = ocr(id,Languages,False)
        if isInError:
            print("Error OCRing Document: "+id)
        else:
            print(Res)


def ocr(DocID,Languages=["eng"],CsvOutput=False):
    LanguagesObjType = type(Languages).__name__ # this should always be a list
    if(LanguagesObjType != "list"): #if somehow the language list is a string.. then let's convert it to a proper list
        Languages = Languages.split(",")

    payload = GET_OCR_BODY(Languages)

    URL = urllib.parse.urljoin(DataUtils.GetUrl(), get_CLASS_OCR_URI(DocID))


    headers = {
        'content-type': "multipart/form-data;",
        'Content-Type': "application/x-www-form-urlencoded",
        'Accept': "*/*",
        'Cache-Control': "no-cache",
    }

    response = requests.request(CLASS_OCR_REQ_TYPE, URL,params=payload, headers=headers)
    Res,isInError = OCRResponses.Process_ocr_Response(response,CsvOutput)

    #print("DEBUG:"+Res)
    return Res,isInError
