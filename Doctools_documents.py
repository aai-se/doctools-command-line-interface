##############################################################
#
# Author: Bren Sapience
# Date: Dec 2019
# Scope: Provide CLI functions around Doctools
#
# Call Examples:
#
#   python3 ./
#   python3 ./
#
##############################################################

import argparse
import sys
sys.path.insert(1, './logics')
import DocumentsLogics
import LanguageLogics
import OCRLogics
sys.path.insert(1, './libs')
import DataUtils

#Allowed Operations - should be lower case
SupportedOperations = ['list','upload','delete','ocr']

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Operation should be available in all modules
    parser.add_argument("-o","--operation", type=str,default = "list", help = "<list,upload,delete,ocr>: documents", dest = "OPERATION")
    parser.add_argument("-d","--documents", type=str,default = "", help = "Documents to Upload to Doctools (csv)", dest = "FILEPATHS")
    parser.add_argument("-g","--languages", type=str,default = "eng", help = "(OCR Only) Comma Seperated List of 3-char Languages (ex: \"eng,fra\")", dest = "LANGUAGES")
    parser.add_argument("-f","--folders", type=str,default = "", help = "Upload all Documents in Folder", dest = "FOLDERPATH")
    parser.add_argument("-i","--ids", type=str,default = "", help = "Document IDs (csv)", dest = "DOCIDS")

    OPTIONS = parser.parse_args()

    #Check that operation is Supported
    if OPTIONS.OPERATION.lower() not in SupportedOperations:
        parser.error('Operation passed (-o) is not supported: '+OPTIONS.OPERATION)
    DataUtils.DisplayURLandPort()
    ###
    # list operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[0]: #list
        DocumentsLogics.list()

    ###
    # upload operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[1]: #upload
        #print("Upload Documents")
        if not OPTIONS.FILEPATHS and not OPTIONS.FOLDERPATH:
            parser.error('You need to pass parameter -d or -f (--documents or --folder)')
        IDs = DocumentsLogics.upload(OPTIONS.FILEPATHS,OPTIONS.FOLDERPATH)
        print(IDs)
    ###
    # delete operation
    ###

    if OPTIONS.OPERATION.lower() == SupportedOperations[2]: #delete
        if not OPTIONS.DOCIDS:
            parser.error('You need to pass parameter -d or (--ids)')

        DocumentsLogics.delete(OPTIONS.DOCIDS)
        #ClassifiersLogics.delete(OPTIONS.CLASSIFIERID)

    ###
    # OCR operation
    ###
    if OPTIONS.OPERATION.lower() == SupportedOperations[3]: #ocr
        #print("OCR Documents")
        if not OPTIONS.DOCIDS:
            parser.error('You need to pass parameter -i or (--ids)')

            LanguagesAllSupported,ProblematicLanguages = LanguageLogics.checkLanguages(OPTIONS.LANGUAGES)
            if not LanguagesAllSupported:
                print("!! Fatal Error: Languages Unsupported: "+str(ProblematicLanguages))
                exit(1)

        OCRLogics.ocr_docs(OPTIONS.DOCIDS,OPTIONS.LANGUAGES)
        #ClassifiersLogics.delete(OPTIONS.CLASSIFIERID)
